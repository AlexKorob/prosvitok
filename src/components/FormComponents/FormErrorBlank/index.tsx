import React from 'react';
import styles from './styles.module.scss';

const FormErrorBlank = ({ height = 24 }: { height?: number }) => {
  return <div style={{ height }} className={styles.blankError} />;
};

export default FormErrorBlank;
