import useWindowDimensions from 'hooks/useWindowDimensions';
import { MOBILE_WIDTH } from 'variables';

export function useIsMobile() {
  const { width } = useWindowDimensions();

  return width < MOBILE_WIDTH;
}
