## Prosvitok single page app

## Developing

1. Install dependencies - run `yarn` from _root_ dir:
   ```bash
   yarn
   ```
2. Copy and fill environment variables:
   ```bash
   cp local_env .env
   ```
3. Start development server:
   ```bash
   yarn start
   ```

## Deploy
```bash
   yarn run build
   aws s3 sync build/ $(echo $S3_BUCKET)
   aws cloudfront create-invalidation --distribution-id $(echo $CLOUD_FRONT_DISTRIBUTION_ID) --paths "/*"
```
